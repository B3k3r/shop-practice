import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

let cart = window.localStorage.getItem('cart');
let prodCount = window.localStorage.getItem('prodCount');

Vue.use(Vuex)

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        cart: cart ? JSON.parse(cart) : [],
        prodCount: prodCount ? parseInt(prodCount) : 0,
    },
    mutations: {
        addToCart(state, item) {
            let found = state.cart.find(product => product.id == item.id);
        
            if (found) {
                found.quantity ++;
                found.totalPrice = found.quantity * found.price;
            } else {
                state.cart.push(item);
        
                Vue.set(item, 'quantity', 1);
                Vue.set(item, 'totalPrice', item.price);
            }
        
            state.prodCount++;
        },
        removeFromCart(state, item) {
            let index = state.cart.indexOf(item);
        
            if (index > -1) {
                let product = state.cart[index];
                state.prodCount -= product.quantity;
        
                state.cart.splice(index, 1);
            } 
        },
        saveCart(state) {
            window.localStorage.setItem('cart', JSON.stringify(state.cart));
            window.localStorage.setItem('prodCount', state.prodCount);
        }     
    },
    getters: {
        getCart: (state) => 
        { 
            return state.cart
        }
    }
})

export default store;
